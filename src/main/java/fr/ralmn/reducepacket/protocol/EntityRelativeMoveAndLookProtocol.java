package fr.ralmn.reducepacket.protocol;

import com.comphenix.protocol.events.ConnectionSide;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import fr.ralmn.reducepacket.ReducePacket;
import net.minecraft.server.v1_6_R3.Packet33RelEntityMoveLook;

/**
 * Created with IntelliJ IDEA.
 * User: ralmn
 * Date: 28/09/13
 * Time: 17:26
 * To change this template use File | Settings | File Templates.
 */
public class EntityRelativeMoveAndLookProtocol extends PacketAdapter {

    private ReducePacket reducePacket;

    public EntityRelativeMoveAndLookProtocol(ReducePacket reducePacket) {
        super(reducePacket, ConnectionSide.BOTH, ListenerPriority.NORMAL, 33);
        this.reducePacket = reducePacket;

    }

    @Override
    public void onPacketSending(PacketEvent packetEvent) {

        if (packetEvent.getPacketID() == 33) {
            if (!reducePacket.seePlayers.contains(packetEvent.getPlayer().getName())) {
                packetEvent.setCancelled(true);
            }
        }
    }
}
