package fr.ralmn.reducepacket.protocol;

import com.comphenix.protocol.events.ConnectionSide;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import fr.ralmn.reducepacket.ReducePacket;

/**
 * Created with IntelliJ IDEA.
 * User: ralmn
 * Date: 28/09/13
 * Time: 17:26
 * To change this template use File | Settings | File Templates.
 */
public class PlayerListItemProtocol extends PacketAdapter {

    private ReducePacket reducePacket;

    public PlayerListItemProtocol(ReducePacket reducePacket) {
        super(reducePacket, ConnectionSide.BOTH, ListenerPriority.NORMAL, 0xC9);
        this.reducePacket = reducePacket;

    }

    @Override
    public void onPacketSending(PacketEvent packetEvent) {

        if (packetEvent.getPacketID() == 0xC9) {
            if (!reducePacket.seePlayers.contains(packetEvent.getPlayer().getName())) {
                packetEvent.setCancelled(true);
            }
        }
    }
}
