package fr.ralmn.reducepacket;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import fr.ralmn.reducepacket.protocol.*;
import net.minecraft.server.v1_6_R3.Packet20NamedEntitySpawn;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_6_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * @author ralmn
 */
public class ReducePacket extends JavaPlugin implements Listener {

    public HashSet<String> seePlayers;
    private ProtocolManager protocolManager;
    private YamlConfiguration config;
    private ArrayList<String> vips = new ArrayList<String>();

    public static Location stringToLocation(final String s) {
        if (s == null || s == "") return null;
        final String[] split = s.split(";");
        final double x = Double.parseDouble(split[0]);
        final double y = Double.parseDouble(split[1]);
        final double z = Double.parseDouble(split[2]);

        final World world = Bukkit.getWorld(split[3]);
        return new Location(world, x, y, z);
    }

    public static String locationToString(final Location l) {
        return l.getX() + ";" + l.getY() + ";" + l.getZ() + ";" + l.getWorld().getName();
    }

    public void onLoad() {

        protocolManager = ProtocolLibrary.getProtocolManager();
    }

    @Override
    public void onDisable() {
        for (Player player : getServer().getOnlinePlayers()) {
            config.getConfigurationSection("player").set(player.getName(), locationToString(player.getLocation()));
            saveFileConfig(player.getName());
        }
    }

    private void saveFileConfig(String name) {
        File f = new File("/home/minecraft/players/" + name + ".yml");

        try {

            if (!f.getParentFile().exists())
                f.mkdirs();
            if (!f.exists()) {
                f.createNewFile();
            }
            config.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void onEnable() {
        vips.add("ralmn");
        vips.add("Zelvac");
        vips.add("zelvac");
        vips.add("richie3366");
        vips.add("r0ro3lectr0");
        seePlayers = new HashSet<String>();


        this.getServer().getPluginManager().registerEvents(this, this);
        protocolManager.addPacketListener(new EntityHeadLookProtocol(this));
        protocolManager.addPacketListener(new EntityProtocol(this));
        protocolManager.addPacketListener(new EntityLookProtocol(this));
        protocolManager.addPacketListener(new EntityRelativeMoveAndLookProtocol(this));
        protocolManager.addPacketListener(new EntityRelativeMoveProtocol(this));
        protocolManager.addPacketListener(new EntityTeleportProtocol(this));
        protocolManager.addPacketListener(new SpawnNamedEntityProtocol(this));
        protocolManager.addPacketListener(new PlayerListItemProtocol(this));
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    @EventHandler
    public void onJoinEvent(PlayerJoinEvent e) {
        Player pl = e.getPlayer();


        if (!vips.contains(pl.getName()))
            e.setJoinMessage(null);

        pl.teleport(checkPlayer(pl));
    }

    @EventHandler
    public void onFoodLevelChangeEvent(FoodLevelChangeEvent e) {
        e.getEntity().setHealth(20.0);
        e.setFoodLevel(20);
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        e.setDeathMessage(null);
    }


    @EventHandler
    public void onQuitEvent(PlayerQuitEvent e) {
        if (!vips.contains(e.getPlayer().getName().toString()))
            e.setQuitMessage(null);

        config.getConfigurationSection("player").set(e.getPlayer().getName(), locationToString(e.getPlayer().getLocation()));
        saveFileConfig(e.getPlayer().getName());
    }

    @EventHandler
    public void onBreakEvent(BlockBreakEvent e) {
        if (!vips.contains(e.getPlayer().getName().toString()))
            e.setCancelled(true);

    }

    @EventHandler
    public void onPlaceEvent(BlockPlaceEvent e) {
        if (!vips.contains(e.getPlayer().getName().toString()))
            e.setCancelled(true);

    }

    @EventHandler
    public void onInteractEvent(PlayerInteractEvent e) {

        if (vips.contains(e.getPlayer().getName().toString()))
            return;

        if (e.getAction().equals(Action.LEFT_CLICK_BLOCK)
                || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (e.getClickedBlock().getType().equals(Material.WOOD_DOOR)
                    || e.getClickedBlock().getType().equals(Material.IRON_DOOR_BLOCK)
                    || e.getClickedBlock().getType().equals(Material.IRON_DOOR)
                    || e.getClickedBlock().getType().equals(Material.WOODEN_DOOR)
                    || e.getClickedBlock().getType().equals(Material.TRAP_DOOR)
                    || e.getClickedBlock().getType().equals(Material.FENCE_GATE)) {
                e.setCancelled(false);
                return;
            }
        }
        e.setCancelled(true);


    }

    @EventHandler
    public void onPlayerIE(PlayerInteractEntityEvent e) {
        if (!vips.contains(e.getPlayer().getName().toString()))
            e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerTalk(AsyncPlayerChatEvent e) {
        if (!vips.contains(e.getPlayer().getName().toString()))
            e.setCancelled(true);

    }

    private void loadConfigFile(String name) {
        File f = new File("/home/minecraft/players/" + name + ".yml");

        if (!f.getParentFile().exists())
            f.mkdirs();

        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
            }
        }
        config = YamlConfiguration.loadConfiguration(f);
        if (!config.isConfigurationSection("player")) {
            config.createSection("player");
            saveFileConfig(name);
        }
    }

    @EventHandler
    public void onPlayerPreposses(PlayerCommandPreprocessEvent e) {
        System.out.println(e.getMessage() + "-" + e.getPlayer().getName().toString() +"-" + vips.contains(e.getPlayer().getName().toString()));
        if(e.getMessage().contains("spawn") ||e.getMessage().contains("see") || e.getPlayer().isOp() ){
            return;
        }
        if (!vips.contains(e.getPlayer().getName().toString())) {
            e.setCancelled(true);
        }
    }

    private Location checkPlayer(final Player pl) {
        Location l = pl.getWorld().getSpawnLocation();
        loadConfigFile(pl.getName());
        if (config.getConfigurationSection("player").contains(pl.getName())) {
            String s = (String) config.getConfigurationSection("player").get(pl.getName());
            l = stringToLocation(s);
        }
        return l;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Player only");
            return true;
        }

        Player player = (Player) sender;

        if (command.getName().equals("see") && (player.isOp() || vips.contains(player.getName().toString()))) {

            if (seePlayers.contains(player.getName())) {
                seePlayers.remove(player.getName());
                player.sendMessage(ChatColor.AQUA + "You can't see players");
            } else {
                seePlayers.add(player.getName());
                player.sendMessage(ChatColor.AQUA + "You can see players");
                CraftPlayer cp = (CraftPlayer) player;
                for (Player pl : getServer().getOnlinePlayers()) {
                    if (pl.getName().equalsIgnoreCase(player.getName())) {
                        continue;
                    }
                    CraftPlayer cpl = (CraftPlayer) pl;
                    Packet20NamedEntitySpawn p = new Packet20NamedEntitySpawn(cpl.getHandle());
                    cp.getHandle().playerConnection.sendPacket(p);
                }

            }
            return true;
        }else if(command.getName().equalsIgnoreCase("spawn")){
            player.teleport(player.getWorld().getSpawnLocation());
        }

        return true;
    }
}
